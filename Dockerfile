# syntax=docker/dockerfile:experimental
FROM node:14.15.0

WORKDIR /app

COPY ["package.json","package-lock.json","./"]

RUN npm install

COPY . .

RUN npm install -g nodemon

CMD nodemon app.js