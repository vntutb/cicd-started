const express = require('express')
const mongoose = require('mongoose')
const userModel = require('./models/userModel')
mongoose.connect('mongodb://mongo:27017/node-test').then(data=>{
    console.log(`Connect database success`)
}).catch(error=>{
    console.log(`Connect database fail`)
})
const app = express()

app.get("/" ,async (req,res)=>{
    let data = await userModel.find();
    return res.status(200).json({
        data: data
    })
})
app.post('/',async(req,res)=>{
    await userModel.create({
        userName:"VuNgocTu",
        password:"abs"
    })
    return res.status(200).json({
        status:"success",
        message:"created data success"
    })
})
app.listen(3000,()=>{
    console.log("Connect to server at port 3000")
})